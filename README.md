# FreeRADIUS Container Management
[![pipeline status](https://gitlab.com/hasechris-docker-images/freeradius-servicemanagement/badges/main/pipeline.svg)](https://gitlab.com/hasechris-docker-images/freeradius-servicemanagement/-/commits/main)

This container serves only one purpose: Start or stop another container based on the selection from the served http site.

## HTTP Server
This container starts a apache2 server with a cgi script. This bash-script shows a brief explanation of this site and the needed buttons to start or stop the guest network authentication via a second freeradius server. See the container on [gitlab](https://gitlab.com/hasechris-docker-images/freeradius-accept-all) or on [docker hub](https://hub.docker.com/r/hasechris92/freeradius-accept-all)

When a user selects the button "start network guest access" this bash script is calling itself via the http form as POST and gives itself the form data. The bash script filters the POST call and executes the start or stop of the named docker container with the freeradius service.

## Security
For this function the container needs access to the docker socket. **Beware!** Giving access to the docker socket is unsafe. Only use this container in an enclosed, non-internet-facing internal network with limited access. Clients who hack the webserver can get access to the dockerhost itself via the docker socket.

## Docker Compose
### Environment Variable
| FREERADIUS_CONTAINER_NAME | Specify the name of the second container |

### Compose File

