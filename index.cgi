#!/bin/bash
timefile="/dynamic/container-stoptime"


echo Content-type: text/html
echo ""
## make POST and GET strings
## as bash variables available
if [ ! -z $CONTENT_LENGTH ] && [ "$CONTENT_LENGTH" -gt 0 ] && [ $CONTENT_TYPE != "multipart/form-data" ]; then
  read -n $CONTENT_LENGTH POST_STRING <&0
  eval `echo "${POST_STRING//;}"|tr '&' ';'`
fi
eval `echo "${QUERY_STRING//;}"|tr '&' ';'`


# get POST Variable and search for action
if [[ "$POST_STRING" == "action=start1" ]]; then
  # start docker container for 30 minutes
  /usr/local/bin/docker container start ${FREERADIUS_CONTAINER_NAME:-freeradius} > /dev/null 2>&1
  /bin/bash -c "sleep 30m; /usr/local/bin/docker container stop ${FREERADIUS_CONTAINER_NAME:-freeradius}" &
  echo "$(date -d '+30 minutes' '+%T')" > $timefile
elif [[ "$POST_STRING" == "action=start2" ]]; then
  # start docker container for 1 hour
  /usr/local/bin/docker container start ${FREERADIUS_CONTAINER_NAME:-freeradius} > /dev/null 2>&1
  /bin/bash -c "sleep 1h; /usr/local/bin/docker container stop ${FREERADIUS_CONTAINER_NAME:-freeradius}" &
  echo "$(date -d '+1 hour' '+%T')" > $timefile
elif [[ "$POST_STRING" == "action=start3" ]]; then
  # start docker container for 4 hours
  /usr/local/bin/docker container start ${FREERADIUS_CONTAINER_NAME:-freeradius} > /dev/null 2>&1
  /bin/bash -c "sleep 4h; /usr/local/bin/docker container stop ${FREERADIUS_CONTAINER_NAME:-freeradius}" &
  echo "$(date -d '+4 hour' '+%T')" > $timefile
elif [[ "$POST_STRING" == "action=stop" ]]; then
  /usr/local/bin/docker container stop ${FREERADIUS_CONTAINER_NAME:-freeradius} > /dev/null 2>&1
  rm $timefile
fi

# get actual status
containerlist=$(docker container ls | grep ${FREERADIUS_CONTAINER_NAME} | wc -l)
if [[ "$containerlist" == 0 ]]; then
  # no container found
  radiusstate="Ausgeschaltet"
  statediv="off"
elif [[ "$containerlist" == 1 ]]; then
  # one container found
  radiusstate="Eingeschaltet"
  statediv="on"
  stoptime="$(cat $timefile)"
else
  # alle anderen Zustände sind ein Fehler
  radiusstate="Fehler"
  statediv="error"
fi

echo '<!DOCTYPE html>'
echo '<html lang="de">'
echo '  <head>'
echo '    <meta charset=\"utf-8\">'
echo '    <meta name="viewport" content="width=device-width, initial-scale=1.0">'
echo '    <link rel="stylesheet" href="../index.css">'
echo '    <title>Gastzugang Senioren-WG</title>'
echo '  </head>'
echo '  <body>'
echo '    <form id="formular" action="index.cgi" method="post">'
echo '      <div class="container">'
echo '        <div class="header">'
echo '          <h1>Gastzugang Senioren-WG</h1>'
echo '        </div>'
echo '        <div class="beschreibung">'
echo '          Hiermit kannst du die Anmeldung im Gastnetz der Senioren-WG aktivieren.<br>&Uuml;ber die Kn&ouml;pfe unten kannst du den Zeitraum w&auml;hlen.<br><br><b>Wichtig:</b> Es geht hierbei nur um die Anmeldung am Gastnetz. Bestehende Verbindungen bleiben dar&uuml;ber hinaus vorhanden.'
echo '        </div>'
if [[ "$containerlist" == 1 ]]; then
  echo '        <div class="Button1">'
  echo '          <button form="formular" name="action" value="start1" class="greybutton" disabled >Anmeldung 30 Minuten freigeben</button>'
  echo '        </div>'
  echo '        <div class="Button2">'
  echo '          <button form="formular" name="action" value="start2" class="greybutton" disabled >Anmeldung 1 Stunde freigeben</button>'
  echo '        </div>'
  echo '        <div class="Button3">'
  echo '          <button form="formular" name="action" value="start3" class="greybutton" disabled >Anmeldung 4 Stunden freigeben</button>'
  echo '        </div>'
  echo '        <div class="Button4">'
  echo '          <button form="formular" name="action" value="stop" class="redbutton" type="submit">Anmeldung ausschalten</button>'
  echo '        </div>'
  echo '        <div class="status">'
  echo "          <button class=\"${statediv}\" type=\"button\">${radiusstate} bis ${stoptime}</button>"
else
  echo '        <div class="Button1">'
  echo '          <button form="formular" name="action" value="start1" class="greenbutton" type="submit">Anmeldung 30 Minuten freigeben</button>'
  echo '        </div>'
  echo '        <div class="Button2">'
  echo '          <button form="formular" name="action" value="start2" class="greenbutton" type="submit">Anmeldung 1 Stunde freigeben</button>'
  echo '        </div>'
  echo '        <div class="Button3">'
  echo '          <button form="formular" name="action" value="start3" class="greenbutton" type="submit">Anmeldung 4 Stunden freigeben</button>'
  echo '        </div>'
  echo '        <div class="Button4">'
  echo '          <button form="formular" name="action" value="stop" class="redbutton" type="submit">Anmeldung ausschalten</button>'
  echo '        </div>'
  echo '        <div class="status">'
  echo "          <button class=\"${statediv}\" type=\"button\">${radiusstate}</button>"
fi
echo '        </div>'
echo '      </div>'
echo '    </form>'
echo '  </body>'
echo '</html>'

